# Skillsharing

**Goal:** this project aims at improving the sharing of skills among the Frontend group.

These efforts should not be a replacement of technical documentation but an addition.

## How to help?

1. Take a look through the issues and see if there is any one unassigned that you would be willing to contribute content for.
1. Step in, assign to yourself and share the plans for your content. 
1. When content is produced and properly linked in the handbook, close the issue.

If you want to contribute with a topic that doesn't have an issue created, create it and follow the above suggested steps.

## Format

* **Length:** 5-10min (shorter ok but not longer)
* **Where:** [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A)
* **How:** Following the [No Extra Work](https://about.gitlab.com/handbook/communication/youtube/#no-extra-work) rule, record off a Zoom call and upload. 


After uploading, make sure the video is linked somewhere in the handbook at least once.

**Note:** A section for this effort shall be created soon and linked here.